package com.xiledsystems.aaltestproject;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import com.xiledsystems.aal.gaming.BaseGameObject;
import com.xiledsystems.aal.gaming.BaseSprite;
import com.xiledsystems.aal.gaming.BitmapSprite;
import com.xiledsystems.aal.gaming.GamePanel;
import com.xiledsystems.aal.gaming.collision.CollisionListener;
import com.xiledsystems.aal.gaming.collision.SimpleRectangle;
import com.xiledsystems.aal.gaming.spritesheet.SpriteSheet;
import com.xiledsystems.aal.util.Rand;
import java.util.ArrayList;
import java.util.Collections;


public class Main extends Activity {

    private GamePanel gamePanel;

    private int curCount = 1;
    private int maxCount = 500;

    private Handler handler;

    private static Bitmap spriteBitmap;
    private static SpriteSheet splosionSheet;

    private boolean rotate = false;

    private CollisionListener collisionListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gamePanel = new GamePanel(this);
        gamePanel.setBackgroundColor(0x00000000);
        gamePanel.showFPS(true);
        gamePanel.setEdgeReachedListener(new GamePanel.EdgeReached() {
            @Override
            public void edgeReached(BaseGameObject object, int edge) {
                ((BaseSprite) object).bounce(edge);
            }
        });
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.container);
        rl.addView(gamePanel);

        handler = new Handler();

        spriteBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ship);
        splosionSheet = new SpriteSheet(this, "spritemap.json", R.drawable.splosion);
        ArrayList<String> order = splosionSheet.frameOrder();
        Collections.sort(order);
        splosionSheet.setFrameOrder(order);


    }

    @Override
    protected void onStart() {
        super.onStart();
        curCount = 0;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                spawnNewSprite();
                if (curCount < maxCount) {
                    handler.postDelayed(this, Rand.rndInt(250, 350));
                }
            }
        }, Rand.rndInt(50, 250));
    }

    private void spawnNewSprite() {
        BitmapSprite sprite = new BitmapSprite(gamePanel);
        sprite.setBitmap(spriteBitmap);
        //if (Rand.rndInt(0, 2) == 0) {
        //    sprite.setWidth(Rand.rndInt((int) (sprite.getWidth() / 2), (int) (sprite.getWidth() * 2)));
        //    sprite.setHeight(sprite.getWidth());
       // }
        sprite.setX(Rand.rndInt(0, 700));
        sprite.setY(Rand.rndInt(0, 1700));
        if (rotate) {
            rotate = false;
            sprite.setAngle(Rand.rndInt(0, 360));
            sprite.setRotates(true);
        } else {
            rotate = true;
        }
        sprite.setSpeed((float) (Math.random() * 20));
        sprite.setEnabled(true);
        sprite.checkForEdges(true);
        sprite.setCollisionListener(new CollideListener(sprite));
        sprite.setTouchListener(new BaseGameObject.TouchListener() {
            @Override
            public void onTouchDown(BaseGameObject object, float x, float y) {
            }

            @Override
            public void onTouchUp(BaseGameObject object, float x, float y) {
                spawnExplosion(object);
            }

            @Override
            public void onDragged(BaseGameObject object, float startX, float startY, float lastX, float lastY, float curX, float curY) {
            }
        });

        curCount++;
    }

    private void spawnExplosion(BaseGameObject object) {
        object.kill();
        BitmapSprite s = new BitmapSprite(gamePanel);
        s.checkForEdges(false);
        s.setLocation(object.getX(), object.getY());
        s.setSpriteSheet(splosionSheet, 4);
        s.setDieAfterAnimation(true);
        s.setEnabled(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        gamePanel.removeAllObjects();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        gamePanel.release();
        super.onDestroy();
    }

    private class CollideListener extends SimpleRectangle {

        private final BitmapSprite sprite;

        public CollideListener(BitmapSprite sprite) {
            this.sprite = sprite;
        }

        @Override
        public void collidedWith(BaseGameObject object) {
            spawnExplosion(sprite);
        }

        @Override
        public void noLongerCollidingWith(BaseGameObject object) {

        }
    }


}
